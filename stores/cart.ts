import { defineStore } from 'pinia';

export const useCartStore = defineStore({
  id: 'cartStore',
  state: () => ({
    loadingShoppingCart: false,
    responseData: {},
    dataShoppingCart: {
      products: []
    },
    dataProductCart: [],
    newAddProduct: {
      products: []
    },
    responseDataDelete: {}
  }),
  getters: {
    getProductCart (state) {
      return state.dataProductCart
    }
  },
  actions: {
    /**
     * Because using dummy API, so adding a new cart will not add it into the server.
     * docs https://dummyjson.com/docs/carts
     */
    async addToCart(payload: Object) {
      this.loadingShoppingCart = true
      try {
        const res = await fetch('https://dummyjson.com/carts/add', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(payload)
        })
        this.responseData = res
        if (res.status === 200) {
          this.newAddProduct = await res.json()
          this.dataProductCart.push(this.newAddProduct.products[0]) //only manipulation with push data
        }
      } catch (error) {
        console.log(error)
      } finally {
        this.loadingShoppingCart = false
      }
    },
    async getCart(userId: Number) {
      try {
        const res = await fetch(`https://dummyjson.com/carts/${userId}`, {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' }
        })
        if (res.status === 200) {
          this.dataShoppingCart = await res.json()
          this.dataProductCart = this.dataShoppingCart.products
        }
      } catch (error) {
        console.log(error)
      } finally {
        this.loadingShoppingCart = false
      }
    },
    /**
     * Because using dummy API, so deleting a cart will not delete it into the server.
     * docs https://dummyjson.com/docs/carts
     */
    async removeCart(productId: number, productIndex: number) {
      this.loadingShoppingCart = true
      try {
        const res = await fetch(`https://dummyjson.com/carts/${productId}`, {
          method: 'DELETE',
          headers: { 'Content-Type': 'application/json' }
        })
        this.responseDataDelete = res
        if (res.status === 200) {
          this.dataProductCart = this.dataProductCart.filter((item: any, index) => index !== productIndex) //only manipulation with remove data
        }
      } catch (error) {
        console.log(error)
      } finally {
        this.loadingShoppingCart = false
      }
    },
    /**
     * Because using dummy API, so updating a cart will not update it into the server.
     * docs https://dummyjson.com/docs/carts
     */
    async updateCart(id: number, payload: any) {
      try {
        await fetch(`https://dummyjson.com/carts/${id}`, {
          method: 'PUT',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(payload)
        })
      } catch (error) {
        console.log(error)
      } finally {
        this.loadingShoppingCart = false
      }
    }
  }
})