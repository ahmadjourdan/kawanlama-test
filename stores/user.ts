import {defineStore} from 'pinia'

export const useUserStore = defineStore({
  id: 'userStore',
  state: () => ({
    loading: false,
    dataUser: {} as any,
    messageError: ''
  }),
  getters: {
    getUser() {
      return localStorage.getItem('user')
    },
    getAuthToken() {
      return localStorage.getItem('token')
    },
    getMessageError(state) {
      return state.messageError
    }
  }, 
  actions: {
    /**
     * Username: kminchelle
     * Password: 0lelplR
     */
    async login(username: String, password: String) {
      const payload = {
        username: username,
        password: password
      }

      this.dataUser = {}
      this.loading = true
      try {
        const res = await fetch('https://dummyjson.com/auth/login', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(payload)
        })
        if (res.status === 200) {
          this.dataUser = await res.json()
          localStorage.setItem('user', JSON.stringify(this.dataUser))
          localStorage.setItem('token', this.dataUser.token)
        } else {
          this.messageError = 'Sorry your username and password does not match'
        }
      } catch (error) {
        console.log(error)
      } finally {
        this.loading = false
      }
    },
    logout() {
      localStorage.clear()
      window.location.reload()
    }
  }
})