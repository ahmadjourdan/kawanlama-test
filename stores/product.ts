import {defineStore} from 'pinia'

export const useProductStore = defineStore({
  id: 'productStore',
  state: () => ({
    loading: false,
    productList: [] as any[],
    responseProductList: {
      products: []
    }
  }),
  getters: {},
  actions: {
    async getProductList(limit: number, skip: number) {
      this.loading = true
      try {
        const res = await fetch(
          `https://dummyjson.com/products?limit=${limit}&skip=${skip}`
        )
        if (res.status === 200) {
          this.responseProductList = await res.json()
          this.productList.push(...this.responseProductList.products)
        }
      } catch (error) {
        console.log(error)
      } finally {
        this.loading = false
      }
    }
  }
})
