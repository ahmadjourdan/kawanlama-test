# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

## Development Server

Start the development server on `http://localhost:3000`

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

## Dummy API

Because using Dummy API, so method [POST], [PUT], [DELETE] not will be updated.
- Adding a new cart will not add it into the server.
- Updating a cart will not update it into the server.
- Deleting a cart will not delete it into the server.

Account Login <br />
**Username**: kminchelle <br />
**Password**: 0lelplR

Check out the (https://dummyjson.com/docs) for how to use it.
